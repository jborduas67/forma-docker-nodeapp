#!/bin/bash

set -e

if [ "$1" = 'start' ]; then
  exec npm start
  exec node bin/www
else [ "$1" = 'debug' ]; then
  DEBUG=true  exec npm start
fi

exec "$@"