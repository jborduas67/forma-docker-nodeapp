FROM node:10

WORKDIR       /app

COPY          docker-entrypoint.sh    /entrypoint.sh
COPY          src                     .
COPY          package.json            .



RUN           npm install
RUN           chmod +x /entrypoint.sh

EXPOSE        8080

ENTRYPOINT    [ "/entrypoint.sh" ]
CMD           [ "start" ]